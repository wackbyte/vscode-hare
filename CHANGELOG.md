# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

### Changed

- Improve syntax highlighting of the declaration-starting keywords `const`, `def`, `fn`, `let`, and `type`.
- Improve syntax highlighting of the modifier keyword `nullable`.

## [0.0.2]

Released on 2024-07-04.

### Added

- Labels (`for :label (true) { ... }`, `break :label`).
- The `done` keyword.

### Fixed

- The order in which different types of operators were matched was incorrect. This may have produced incorrect highlighting under certain themes.

## [0.0.1]

Released on 2024-02-21.

### Added

- Basic syntax highlighting.

[Unreleased]: https://codeberg.org/wackbyte/vscode-hare/compare/v0.0.2...trunk
[0.0.2]: https://codeberg.org/wackbyte/vscode-hare/releases/tag/v0.0.2
[0.0.1]: https://codeberg.org/wackbyte/vscode-hare/releases/tag/v0.0.1

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
