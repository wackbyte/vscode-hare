# vscode-hare

Syntax highlighting for [Hare](https://harelang.org/).

## Install

The extension is available as `wackbyte.hare` from the [VS Code Marketplace][vscm] and [Open VSX][ovsx].

You may also install it from [the VSIX package][releases].

## Contribute

Feel free to open an [issue][issues] or [pull request][pulls]!

## License

I release this extension into the public domain under the [Unlicense](https://unlicense.org/).

[issues]: https://codeberg.org/wackbyte/vscode-hare/issues
[pulls]: https://codeberg.org/wackbyte/vscode-hare/pulls
[releases]: https://codeberg.org/wackbyte/vscode-hare/releases
[vscm]: https://marketplace.visualstudio.com/items?itemName=wackbyte.hare
[ovsx]: https://open-vsx.org/extension/wackbyte/hare
